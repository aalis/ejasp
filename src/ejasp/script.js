 /*
 / ejasp - Embedded Javascript Audio Stream Player
 / https://gitlab.com/aalis/ejasp
 / Free Icecast audio player designed for Las Pegasus radio. (https://laspegas.us/)
 / This project is licensed under the MIT License (https://mit-license.org/)
*/
// --- -- -> Configuration <- -- ---

// Address of Icecast audio stream
const EJASP_STREAM_URL = "https://icecast.manehattan.net.laspegas.us:8009/central";

// Address of Icecast's status-json file. Usually it is under /status-json.xsl
const EJASP_ICESTATS_XSL = "https://icecast.manehattan.net.laspegas.us:8009/status-json.xsl";

// Refresh stream information every X miliseconds (1s == 1000ms)
const EJASP_XSL_REFRESH_TIME = 2500;

// Audio type, eg. audio/mpeg for MP3 or audio/ogg for OGG. Incorrect value can result
// in stream being unplayable in older browsers.
const EJASP_MEDIA_TYPE = "audio/mpeg";

// Stick to the bottom of the screen instead of embedding inside a DIV.
const EJASP_STICKY = false;

// Paths to the play/pause/load button pictures.
const EJASP_PATH = {
	"IMG_PLAY":          "ejasp/img/play.png",
	"IMG_PAUSE":         "ejasp/img/pause.png",
	"IMG_LOAD":          "ejasp/img/load.png",
	"IMG_DEFAULT_COVER": "ejasp/img/default_cover.png",
};

// Localization for error messages
const EJASP_MESG_L10N = {
	"LPAPI_NOJSON_ERR": '[EJASP] LPAPI did not returned JSON content.',
	"MOUNTPOINT_DOWN":  'Mountpoint is offline',
	"NO_STREAM_TITLE":  'Stream title not provided',
}

// Remove extensions from audio name.
const EJASP_STREAM_TITLE_DROP_EXT = true;

// Las Pegasus API - https://github.com/tlpr/api
const LPAPI_ENABLE = false;
const LPAPI_URL = "https://api.laspegas.us/";

const LPAPI_DISPLAY_COVER = true;   //it will display default cover the entire time if LPAPI_ENABLE = false
const LPAPI_DISPLAY_RATING = false;
// -- end of API

// Try to find artist in a stream title when not downloading data from LPAPI,
// may cause longer delay in displaying due to regex and wrong detection
const EJASP_ATTEMPT_ARTIST_DETECTION = false;

// Volume used after the page has loaded
const EJASP_DEFAULT_VOLUME = 40;

const EJASP_VERSION = 'b1';

// >- --- -- - -- --- -- - -- --- -

// Global variables
var ejasp_sinfo = {};

// Preload images
if (typeof Image !== undefined)
{
	(new Image()).src = EJASP_PATH.IMG_LOAD;
	(new Image()).src = EJASP_PATH.IMG_PAUSE;
};

// Prevent Firefox from playing audio from cache by appending random GET argument
function cacheBustStreamUrl (raw_stream_url)
{

	nocache_number = new Date().getTime();
	return (EJASP_STREAM_URL + "?noCache=" + nocache_number);

}


function getSongInfoFromLpapi()
{

	var XHR = new XMLHttpRequest();

	var audio_now_uri = 'audio/now.php';
	var request_url = LPAPI_URL + audio_now_uri;

	XHR.open('GET', request_url, true);
	XHR.addEventListener('load', function(aEvt){

		var content_type = this.getResponseHeader('Content-Type');
		if ( (this.status !== 200) || (content_type !== 'application/json') )
		{
			console.log(EJASP_MESG_L10N.LPAPI_NOJSON_ERR);
			return;
		}

		ejasp_sinfo = JSON.parse(this.responseText);

	});
	XHR.send(null);

}


function updateIcestats()
{

	var XHR = new XMLHttpRequest();
	XHR.open('GET', EJASP_ICESTATS_XSL, true);
	XHR.addEventListener('load', function(aEvt){

		if (this.status !== 200)
			return;

		var icestats = JSON.parse(this.responseText).icestats;
		if (icestats.source === undefined)
		{
			ejasp_sinfo.current = {
				'artist': false,
				'title': EJASP_MESG_L10N.MOUNTPOINT_DOWN,
				'cover_art': false,
				'listeners': 0
			}

			return;
		}

		// Handle weird status-json.xsl behaviour
		icestats.sourceCount = ((icestats.source.listeners === undefined) ? icestats.source.length : 1);
		icestats.requestedMountpointExist = false;
		icestats.requestedMountpointUri = EJASP_STREAM_URL.split('/').pop();

		if (icestats.source[0] !== undefined)
		{
			for (i = 0; i < icestats.sourceCount; i++)
			{
				found_mountpoint = icestats.source[i].listenurl.split('/').pop();
				if (icestats.requestedMountpointUri === found_mountpoint)
				{
					icestats.source = icestats.source[i];
					icestats.requestedMountpointExist = true;
					break;
				}
			}
		}
		else
		{
			var found_mountpoint = icestats.source.listenurl.split('/').pop();
			if (icestats.requestedMountpointUri === found_mountpoint)
				icestats.requestedMountpointExist = true;
		}

		// --- -- ---
		// Simulate LPAPI

		if (icestats.source.title === undefined)
		{
			ejasp_sinfo.current = {
				'artist': false,
				'title': EJASP_MESG_L10N.NO_STREAM_TITLE,
				'cover_art': false,
				'listeners': icestats.source.listeners
			}
		}

		else
		{

			ejasp_sinfo.current = {
				'artist': false,
				'title': icestats.source.title,
				'cover_art': false,
				'listeners': icestats.source.listeners
			}

			// Remove file extension
			if (EJASP_STREAM_TITLE_DROP_EXT)
				ejasp_sinfo.current.title = ejasp_sinfo.current.title.replace(/\.[^/.]+$/, "");

			// Try to extract artist from filename
			if (EJASP_ATTEMPT_ARTIST_DETECTION)
			{

				// Check if stream title matches "Artist - Title" syntax, if not then return
				if ( ejasp_sinfo.current.title.match(/^.+\ \-\ .+$/i) === null )
					return;

				var data_array = ejasp_sinfo.current.title.split(' - '),
					artist = data_array[0], song_title = data_array[1];

				ejasp_sinfo.current.artist = artist;
				ejasp_sinfo.current.title = song_title;

			}

		}

		ejasp_sinfo.previous = [];
		ejasp_sinfo.requestedMountpointExist = icestats.requestedMountpointExist;

	});
	XHR.send(null);

}


window.addEventListener('load', function() {

	// Build main ejasp element
	var ejasp = document.getElementById('ejasp');
	ejasp.style.display = 'block';
	ejasp.style.position = (EJASP_STICKY ? 'fixed' : 'relative');
	ejasp.style.borderTop = (EJASP_STICKY ? 'solid 8px #232526' : 'none');

	// Build maximize button
	var ejasp_maximize_btn = document.createElement('div');
	ejasp_maximize_btn.setAttribute('class', 'ejasp_maximize_btn');

	ejasp_maximize_btn.addEventListener('click', function(){
		ejasp.style.display = 'block';
	});

	// -- Pulse effect for maximize button
	var ejasp_maximize_btn_pulse = document.createElement('div');
	ejasp_maximize_btn_pulse.setAttribute('class', 'ejasp_maximize_btn_pulse');

	// Build minimize button
	var ejasp_minimize_btn = document.createElement('div');
	ejasp_minimize_btn.appendChild(document.createTextNode("\u2013")); // endash unicode
	ejasp_minimize_btn.setAttribute('class', 'ejasp_minimize_btn');

	ejasp_minimize_btn.addEventListener('click', function(){
		ejasp.style.display = 'none';
	});

	// Append minimize/maximize buttons
	if (EJASP_STICKY)
	{
		// Maximize & pulse effect
		document.body.appendChild(ejasp_maximize_btn_pulse);
		document.body.appendChild(ejasp_maximize_btn);
		// Minimize
		ejasp.appendChild(ejasp_minimize_btn);
	}

	// Create stream information element
	var ejasp_stream_info_div = document.createElement('div');
	ejasp_stream_info_div.setAttribute('id', 'ejasp_streamtitle');
	ejasp_stream_info_div.style.color = 'white';

	// Song artist
	var ejasp_stream_artist = document.createElement('span');
	ejasp_stream_artist.style.color = 'lightgrey';
	ejasp_stream_artist.style.display = 'block';
	ejasp_stream_artist.style.fontSize = '.8em';

	ejasp_stream_artist.text_node = document.createTextNode('');
	ejasp_stream_artist.appendChild(ejasp_stream_artist.text_node);

	// Song title
	var ejasp_stream_title = document.createElement('span');
	ejasp_stream_title.text_node = document.createTextNode('...');
	ejasp_stream_title.appendChild(ejasp_stream_title.text_node);

	// Put song title and song artist into stream info
	ejasp_stream_info_div.appendChild(ejasp_stream_artist);
	ejasp_stream_info_div.appendChild(ejasp_stream_title);

	// Build container for coverart that is containing ejasp_playpause_button
	var ejasp_btn_and_cover_container = document.createElement("div");
	ejasp_btn_and_cover_container.setAttribute('class', 'ejasp_btn_cover_container');

	// Build cover art and container for ejasp_playpause_button
	var ejasp_coverart = document.createElement('div');
	ejasp_coverart.setAttribute('class', 'ejasp_coverart');

	if (LPAPI_DISPLAY_COVER)
		ejasp_coverart.style.background = 'url("' + EJASP_PATH.IMG_DEFAULT_COVER + '")';

	ejasp_coverart.style.backgroundSize = 'cover';


	var previous_song = '';
	// Retrieve information about stream and update it every 4 seconds
	(function StreamInfoUpdateLoop(){

		if (LPAPI_ENABLE)
			getSongInfoFromLpapi();
		else
			updateIcestats();

		// --- ---

		if (!ejasp_sinfo || (ejasp_sinfo.current === undefined))
		{
			ejasp_stream_artist.text_node.textContent = '';
			ejasp_stream_title.text_node.textContent = '...';
		}

		else if (previous_song !== ejasp_sinfo.current.title)
		{

			ejasp_stream_artist.text_node.textContent = (ejasp_sinfo.current.artist ? ejasp_sinfo.current.artist : '');
			ejasp_stream_title.text_node.textContent = ejasp_sinfo.current.title;

			if (LPAPI_DISPLAY_COVER)
			{
				ejasp_coverart.style.background = 'url("' + (
					ejasp_sinfo.current.cover_art ? ejasp_sinfo.current.cover_art : EJASP_PATH.IMG_DEFAULT_COVER
				) + '")';
				ejasp_coverart.style.backgroundSize = 'cover';
			}

			previous_song = ejasp_sinfo.current.title;

		}

		// --- ---

		setTimeout(StreamInfoUpdateLoop, EJASP_XSL_REFRESH_TIME);

	})();

	// Build play/pause button element and put it inside coverart
	var ejasp_playpause_button = document.createElement("img");
	ejasp_playpause_button.setAttribute("src", EJASP_PATH.IMG_PLAY);
	ejasp_playpause_button.setAttribute("alt", "Play/Pause");
	ejasp_playpause_button.setAttribute("class", "ejasp_playpause");
	ejasp_playpause_button.state = 0;

	ejasp_coverart.appendChild(ejasp_playpause_button);

	// Build audio player element
	var ejasp_audio = document.createElement("audio");
	ejasp_audio.setAttribute("preload", "none");
	ejasp_audio.setAttribute("autobuffer", false); // "preload" for older HTML5 browsers
	ejasp_audio.setAttribute("volume", (EJASP_DEFAULT_VOLUME / 100));
	ejasp_audio.volume = (EJASP_DEFAULT_VOLUME / 100);
	
	ejasp_audio.style.display = "none";
	ejasp_audio.previous_volume = ejasp_audio.volume;
	ejasp_audio.fading_in_progress = false;

	var ejasp_audio_source = document.createElement("source");
	ejasp_audio_source.setAttribute("src", cacheBustStreamUrl(EJASP_STREAM_URL));
	ejasp_audio_source.setAttribute("type", EJASP_MEDIA_TYPE);

	ejasp_audio.appendChild(ejasp_audio_source);

	// Add event on playback start
	ejasp_audio.addEventListener('playing', function(){
	
		this.fading_in_progress = true;
		this.volume = 0;

		ejasp_playpause_button.state = 1;
		ejasp_playpause_button.setAttribute('src', EJASP_PATH.IMG_PAUSE);
		if (LPAPI_DISPLAY_COVER)
			ejasp_playpause_button.setAttribute('class', 'ejasp_playpause ejasp_btn_playing');

		// Fade in
		var timeout_delay = 220;
		(function FadeIn(){
			setTimeout(function(){

				if (!ejasp_audio.fading_in_progress) return; // stop fading if interrupted
				ejasp_audio.volume = (
					( (ejasp_audio.volume * 100) + 5 ) / 100
				); // bypass calculation bug

				if ( ejasp_audio.volume >= ejasp_audio.previous_volume )
				{
					ejasp_audio.fading_in_progress = false;
					return;
				}
				else
					FadeIn();

			}, (timeout_delay - (ejasp_audio.volume * 2)) );
		})();

	});

	// Add events to play/pause button
	ejasp_playpause_button.addEventListener('click', function(){
	
		switch (this.state)
		{
			case 0: // Paused -> Play
				this.state = 2; // set state to loading
				this.setAttribute("src", EJASP_PATH.IMG_LOAD);

				ejasp_audio.play();
				break;
			// case 0 end

			case 1: // Play -> Paused
				this.setAttribute("src", EJASP_PATH.IMG_PLAY);
				this.setAttribute("class", "ejasp_playpause");

				if (ejasp_audio.fading_in_progress)
					ejasp_audio.fading_in_progress = false;
				else
					ejasp_audio.previous_volume = ejasp_audio.volume;
				ejasp_audio.pause();

				// Reload stream
				ejasp_audio_source.setAttribute('src', cacheBustStreamUrl(EJASP_STREAM_URL));

				ejasp_audio.volume = ejasp_audio.previous_volume;
				this.state = 0;

				return;
			// case 1 end

			// case 2 ignore
		}

	});

	// Detect if audio has been paused while browser is minimized (issue #1)
	document.addEventListener("visibilitychange", function() {
	
		var page_hidden = (document.hidden || (document.visibilityState === "hidden"));
		var button_state_match = (ejasp_audio.paused == !ejasp_playpause_button.state);

		if (!page_hidden && !button_state_match)
		{
			ejasp_playpause_button.state = (!ejasp_audio.paused ? 1 : 0); // reverse bool and convert it to int
			if ( ejasp_audio.paused )
				ejasp_playpause_button.setAttribute("src", EJASP_PATH.IMG_PLAY);
			else
				ejasp_playpause_button.setAttribute("src", EJASP_PATH.IMG_PAUSE);
		}

	});

	// Build container for slider, song title, album cover and vote controls
	var ejasp_controls_container = document.createElement("div");
	ejasp_controls_container.style.width = "auto";
	ejasp_controls_container.style.height = "auto";
	ejasp_controls_container.style.display = "inline-block";
	ejasp_controls_container.style.verticalAlign = "middle";

	// Build volume slider element
	var ejasp_volume_control = document.createElement("input");
	ejasp_volume_control.setAttribute("type", "range");
	ejasp_volume_control.setAttribute("min", 0);
	ejasp_volume_control.setAttribute("max", 100);
	ejasp_volume_control.setAttribute("value", EJASP_DEFAULT_VOLUME);
	ejasp_volume_control.style.marginLeft = '0'; // fixes weird 8px margin in Pale Moon

	// Volume change event
	['change', 'input'].forEach(function(evt){
		// oninput should be used instead of onchange to get updates
		// in realtime during dragging, yet some browsers don't support it
		// so let's just listen to both events.
		ejasp_volume_control.addEventListener(evt, function(){
			ejasp_audio.volume = (this.value / 100);
			ejasp_audio.fading_in_progress = false;
		});
	});

	// Build a container for volume bar and stream information
	var ejasp_volume_streaminfo_container = document.createElement('div');
	ejasp_volume_streaminfo_container.style.display = 'inline-block';
	ejasp_volume_streaminfo_container.style.verticalAlign = 'middle';

	ejasp_volume_streaminfo_container.appendChild(ejasp_volume_control);
	ejasp_volume_streaminfo_container.appendChild(ejasp_stream_info_div);

	// Put coverart inside a button container and volume with stream information to
	// the controls container
	ejasp_btn_and_cover_container.appendChild(ejasp_coverart);
	ejasp_controls_container.appendChild(ejasp_volume_streaminfo_container);

	// Add built elements as children to ejasp
	ejasp.appendChild(ejasp_audio);
	ejasp.appendChild(ejasp_btn_and_cover_container);
	ejasp.appendChild(ejasp_controls_container);

});

