![ejasp](design/ejasp.png)
### Embedded Javascript Audio Stream Player

##### Deploying:
- Clone this repository
- Move "ejasp" directory in src/ near your chosen html file
- Add to \<head\>:
```
<script type='text/javascript' src='ejasp/script.js'></script>
<link rel='stylesheet' type='text/css' media='all' href='ejasp/style.css' />
```
- Add to \<body\>:
```
<div id='ejasp'></div>
```
- Done.

##### Compatibility check:
Testing environment: http protocol only, audio/ogg
- Opera 12.18 - OK, but has delayed .playing event. requires OGG audio
- Firefox 24+, including Pale Moon and K-Meleon - OK
- Chrome 8+ - OK
IE, Edge (EdgeHTML) and Safari are going to be tested eventually.

